<?php


abstract class MyAbstractClass{

    protected $name;
    protected $id;

    abstract public function getdata();
    abstract public function setdata($value,$id);
    public function display()
    {
        echo "name:".$this->name."<br>";
        echo "seid:".$this->id;
    }

}
class MyClass extends MyAbstractClass{
    public function getdata()
    {
        return $this->name;
    }
    public function setdata($value,$id)
    {
        $this->name=$value;
        $this->id=$id;
    }
}
$o=new MyClass();
$o->setdata("bitm","123344");
echo $o->display();
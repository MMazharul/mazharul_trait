<?php

interface CanFly{

    public function canFly();
}

interface CanSwim{

    public function canSwim();
}



class Bird
{

    public function Info(){

        echo "I'm mainly a Bird <br>";
        echo "I'm a {{".$this->name."}} <br>";
    }

}

class Penguin extends Bird implements CanSwim{

    protected $name= "Penguin";

    public function canSwim()
    {
        echo "I can Swim <br>";
    }

} //End of Penguin Class


class Dove extends Bird implements CanFly{

    protected $name= "Dove";


    public function canFly()
    {
        echo "I can Fly <br>";

    }


}

class Duck extends Bird implements CanFly,CanSwim{


    protected $name= "Duck";


    public function canFly()
    {
        echo "I can Fly <br>";

    }


    public function canSwim()
    {
        echo "I can Swim <br>";
    }
}



function describe($objBird){


    $objBird->info();



    if($objBird instanceof CanFly)
        $objBird->canFly();


    if($objBird instanceof CanSwim)
        $objBird->canSwim();
    echo "<hr>";
}


describe(new Penguin());
describe(new Dove());
describe(new Duck());
